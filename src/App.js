import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
// import Editoria from './Editoria';
import Editoria from './Editor/Editor';

function App() {
  const [content, setContent] = useState('');

  const getContent = source => {
    setContent(savedContent => `${savedContent} ${source}`);
  };


  return (
    <>
      {/* config={{
      MenuService: [
        {
          templateArea: 'topBar',
          toolGroups: ['Base', 'FullScreen'],
        },
      ],

      SchemaService: DefaultSchema,
      services: [new BaseService(), new FullScreenService(), new ],
    }} */}
     
     {/* <Wax
      // config={config}
      config={config}
      layout={EditoriaLayout}
      value=""
     /> */}
     {/* <Editoria/> */}
     <Editoria/>
     
     {/* <NCBI/> */}
    </>
  );
}
export default App;
