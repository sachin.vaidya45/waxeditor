import React, { useLayoutEffect, useState, useMemo, useRef } from 'react';

import { Wax } from 'wax-prosemirror-core';

import { debounce } from 'lodash';
import EditoriaLayout from './layout/EditoriaLayout';
import config from './config/config';
import { demo } from './demo';
import 'wax-prosemirror-core/dist/index.css'
import 'wax-prosemirror-services/dist/index.css'
import 'wax-table-service/dist/index.css'

const renderImage = file => {
  const reader = new FileReader();
  return new Promise((accept, fail) => {
    reader.onload = () => accept(reader.result);
    reader.onerror = () => fail(reader.error);
    // Some extra delay to make the asynchronicity visible
    setTimeout(() => reader.readAsDataURL(file), 150);
  });
};

const user = {
  userId: 'b3cfc28e-0f2e-45b5-b505-e66783d4f946',
  userColor: {
    addition: 'royalblue',
    deletion: 'indianred',
  },
  username: 'admin',
};

// const users = [{
//   userId: 'b3cfc28e-0f2e-45b5-b505-e66783d4f946',
//   username: 'admin',
// }];

const Editoria = () => {
  const [width] = useWindowSize();

  let layout = EditoriaLayout;
  let key = 'editoria';

  const val = {"type":"doc","content":[{"type":"paragraph","attrs":{"id":"","class":"paragraph","track":[],"group":"","viewid":""},"content":[{"type":"text","text":"asdfsadf asdfsdaf"}]},{"type":"paragraph","attrs":{"id":"","class":"paragraph","track":[],"group":"","viewid":""}},{"type":"heading4","attrs":{"id":"","track":[],"group":"","viewid":""},"content":[{"type":"text","text":"Hello"}]}]};


  const editorRef = useRef();

  const EditoriaComponent = useMemo(
    () => (
      <>
        <Wax
          ref={editorRef}
          key={key}
          config={config}
          autoFocus
          value={{
            "type": "doc",
            "content": [
                {
                    "type": "title",
                    "attrs": {
                        "level": 1,
                        "id": "",
                        "track": [],
                        "group": "",
                        "viewid": ""
                    },
                    "content": [
                        {
                            "type": "text",
                            "text": "Hello"
                        }
                    ]
                },
                {
                    "type": "paragraph",
                    "attrs": {
                        "id": "",
                        "class": "paragraph",
                        "track": [],
                        "group": "",
                        "viewid": ""
                    },
                    "content": [
                        {
                            "type": "text",
                            "text": "World"
                        }
                    ]
                }
            ]
        }}
          placeholder="Type Something..."
          fileUpload={file => renderImage(file)}
          //readonly
          layout={layout}
          targetFormat='JSON'
          onChange={debounce(source => {
            console.log(source);
          }, 200)}
          // user={user}
          // scrollMargin={200}
          // scrollThreshold={200}
        />
      </>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [layout,config],
  );
  return <>{EditoriaComponent}</>;
};

function useWindowSize() {
  const [size, setSize] = useState([window.innerWidth, window.innerHeight]);
  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth, window.innerHeight]);
    }
    window.addEventListener('resize', updateSize);
    updateSize();

    return () => window.removeEventListener('resize', updateSize);
  }, []);
  return size;
}

export default Editoria;
